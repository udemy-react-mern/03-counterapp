import React from 'react';
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';

import PrimeraApp from '../PrimeraApp';


describe('Pruabas de <PrimeraApp />', () => {
  // test('debe mostrar el mensaje Hola Pauli ', () => {
  //   const saludo = 'Hola Pauli';
  //   const { getByText } = render( <PrimeraApp saludo={saludo} />);

  //   expect(getByText(saludo)).toBeInTheDocument();
  // });
  test('debe mostrar el mensaje Hola Pauli ', () => {
    const saludo = 'Hola Pauli';
    const wrapper = shallow(<PrimeraApp saludo={saludo} />);

    expect(wrapper).toMatchSnapshot();
  });

  test('debe mostrar el subtitulo enviado por props ', () => {
    const saludo = 'Hola Pauli';
    const subtitulo = 'Aqui hay un subtitulo';
    const wrapper = shallow(
      <PrimeraApp 
        saludo={saludo} 
        subtitulo={subtitulo} 
      />
    );

    const textParrafo = wrapper.find('p').text();
    expect(textParrafo).toBe(subtitulo);
  });
});
