import '@testing-library/jest-dom';
import React from 'react';
import { shallow } from 'enzyme';
import CounterApp from '../CounterApp';

describe('CounterApp', () => {
  let wrapper = shallow(<CounterApp value={10} />);

  beforeEach( () => {
    wrapper = shallow(<CounterApp value={10} />);
  });

  test('debe crear un snapshot del componente', () => {
    expect(wrapper).toMatchSnapshot();
  });
  test('debe mostrar el valor por default que es 0 ', () => {
    const wrapper = shallow(
      <CounterApp />
    );

    const textParrafo = wrapper.find('h2').text().trim();
    expect(textParrafo).toBe('0');
  });

  describe('eventos de botones: ', () => {
    test('debe incrementar el boton  +1', () => {
      const button = wrapper.find('button').at(0).simulate('click');
      const textParrafo = wrapper.find('h2').text().trim();
      expect(textParrafo).toBe('11');
      
    });
    test('debe decrementar el boton  -1', () => {
      const button = wrapper.find('button').at(1).simulate('click');
      const textParrafo = wrapper.find('h2').text().trim();
      expect(textParrafo).toBe('9');
    });
    test('debe resetear valor', () => {
      wrapper.find('button').at(0).simulate('click');
      wrapper.find('button').at(0).simulate('click');
      wrapper.find('button').at(0).simulate('click');
      const button = wrapper.find('button').at(2).simulate('click');
      const textParrafo = wrapper.find('h2').text().trim();
      expect(textParrafo).toBe('10');
    });
  });
});

