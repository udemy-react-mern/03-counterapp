import '@testing-library/jest-dom';
import { getHeroeById, getHeroesByOwner } from '../../base/08-imp-exp';
import heroes from '../../data/heroes';

describe('Pruebas en archivo 08-imp-exp.js', () => {
  describe('el método getHeroeById', () => {
    test('debe regresar un héroe con un id existente', () => {
      const id = 1;
      const heroe = getHeroeById(id);
      const heroeTest = heroes.find( h => h.id === id);
      expect(heroe).toStrictEqual(heroeTest);
    });
    test('debe regresar undefinded con un id que no existe', () => {
      const id = 10;
      const heroe = getHeroeById(id);
      expect(heroe).toBeUndefined();
    });
  });
  describe('el metodo getHeroesByOwner', () => {
    test('debe regresar un arregle con los heroes de DC', () => {
      const owner = 'DC';
      const heroesData = heroes.filter( h => h.owner === owner);
      const heroesTest = getHeroesByOwner(owner);
      expect(heroesTest).toEqual(heroesData);
    });
    test('debe regresar un arregle con los heroesde Marvel', () => {
      const owner = 'Marvel';
      const heroesData = heroes.filter( h => h.owner === owner);
      const heroesTest = getHeroesByOwner(owner);
      expect(heroesTest).toEqual(heroesData);
      expect(heroesTest.length).toBe(2);
    });
  });
});