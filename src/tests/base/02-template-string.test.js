import '@testing-library/jest-dom';
import { getSaludo } from '../../base/02-template-string';

describe('Pruebas en el archivo 02-template-string.js', () => {
  test('getSaludo debe retornar hola Paulina', () => {
    const nombre = 'Paulina';
    const saludo = getSaludo(nombre);
    
    expect(saludo).toBe(`Hola ${nombre}`);
  });

  test('getSaludo debe retornar hola Pau si no hay argumento en el nombre', () => {
    const saludo = getSaludo();
    expect(saludo).toBe(`Hola Pau`);
  });
})
