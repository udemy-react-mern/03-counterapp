import '@testing-library/jest-dom';
import { getUser, getUsuarioActivo } from '../../base/05-funciones';

describe('Pruebas en 05-funciones.js', () => {
  test('El metodo getUser debe de retornar un objeto', () => {
    const userTest = {
      uid: 'ABC123',
      username: 'paaulinagp'
    };

    const user = getUser();

    // Para evaluar objetos se debe usar: toStrictEqual
    expect(user).toStrictEqual(userTest);
  });

  test('El metodo getUsuarioActivo debe regresar un objeto con el nombre de parametro', () => {
    const userTest = {
      uid: 'ABC123',
      username: 'paaulinagp'
    };

    const user = getUsuarioActivo('paaulinagp');

    expect(user).toStrictEqual(userTest);
  });
});
