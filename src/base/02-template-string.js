


const nombre   = 'Paulina';
const apellido = 'Guerrero';


// const nombreCompleto = nombre + ' ' + apellido;
const nombreCompleto = `${ nombre } ${ apellido }`;

export function getSaludo(nombre = 'Pau') {
    return 'Hola ' + nombre;
}
