import React, { useState } from 'react';
import PropTypes from 'prop-types';


const CounterApp = ({ value = 0 }) => {

  const [ counter, setCounter ] = useState(value);


  //handleAdd
  const handleAdd = () => setCounter(counter+1);
  const handleLess = () => setCounter(counter-1);
  const handleReset = () => setCounter(value);
  
  return (
    <>
      <h1>CounterApp</h1>
      <h2>{ counter }</h2>
      <button onClick = { handleAdd } > + 1 </button>
      <button onClick = { handleLess } > -1 </button>
      <button onClick = { handleReset } > Reset </button>
    </>
  );
};

CounterApp.protoTypes = {
  value: PropTypes.number.isRequired
}

export default CounterApp;

