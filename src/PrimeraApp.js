import React from 'react';
import PropTypes from 'prop-types';

// Functional componet
/**
 * todo contenido envolverlo en un div padre --> <div> <h1></h1> ....</div>
 * usar Fragment <Fragment>
 * usar <> version corta de Fragment no se debe importar
 */
/**
 * Se realiza una destructuración de props que son las propiedades de entrada
 */
const PrimeraApp = ({ saludo , subtitulo }) => {

  return (
    <>
      <h1> { saludo } </h1>
      <p>{ subtitulo}</p>
    </>
  );
};

PrimeraApp.propTypes = {
  saludo: PropTypes.string.isRequired
}

PrimeraApp.defaultProps = {
  subtitulo: 'Soy un subtítulo :D'
}

export default PrimeraApp;